<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * One to one relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function UserDetail()
    {
        return $this->hasOne('App\Models\UserDetail', 'user_id', 'id');
    }
}
