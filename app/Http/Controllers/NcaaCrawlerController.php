<?php

namespace App\Http\Controllers;

use App\Models\Player;
use App\Models\Team;
use Illuminate\Http\Request;
use Goutte\Client;

class NcaaCrawlerController extends Controller
{
    public function index()
    {
        $divisions = [1, 2, 3];
        $roster = array();

        $client = new Client();

        $teamCrawler = $client->request('GET','https://hillarywhiterabbit.deviantart.com/gallery/')->filter('.folderview-art a img')->each(function ($td, $i) {
            return trim($td->attr('src'));
        });

        dd($teamCrawler);

        foreach ($divisions as $division) {

            $teamCrawler = $client->request('GET', 'http://stats.ncaa.org/team/inst_team_list?academic_year=2018&conf_id=-1&division=' . $division . '&sport_code=MBA');

            $teams[$division] = $teamCrawler->filter(' table tr td')->filter('table tr td')->each(function ($td, $i) use (&$division, $roster) {
                $ncaaStatsUrl = 'http://stats.ncaa.org';
                $teamDetailUrl = $td->filter('a')->attr('href');

                $teamName = trim($td->attr('src'));
                dd($teamName);
                /* return [
                                   'name' => trim($td->text()),
                                   'division' => $division,
                                   'url' => $td->filter('a')->attr('href')
                               ];*/

                // Save team's info here
                $teamId = Team::create([
                    'name' => $teamName,
                    'division' => $division,
                    'url' => $teamDetailUrl
                ])->id;

                $rosterUrl = str_replace(basename($teamDetailUrl), 'roster', $teamDetailUrl) . '/' . basename($teamDetailUrl);

                $client = new Client();
                $playerCrawler = $client->request('GET', $ncaaStatsUrl . $rosterUrl);

                $playerHeader = ['jersey', 'name', 'position', 'year', 'gp', 'gs'];

                $playerTable = $playerCrawler->filter('#stat_grid tbody')->filter('tr')->each(function ($tr, $i) {
                    return $tr->filter('td')->each(function ($td, $i) {
                        return trim($td->text());
                    });
                });

                foreach ($playerTable as $key => $item) {

                    $player = array_combine($playerHeader, array_values($item));
                    $player['team_id'] = $teamId;

                    Player::create($player);
                    $roster[$teamName][$key] = $player;

                }
                // 1 team + players added
            });
        }

        return json_encode(['status' => 'SUCCEED']);
    }
    /*
        public function player()
        {
            $client = new Client();

            $crawler = $client->request('GET', 'https://stats.ncaa.org/team/1199/roster/12973');

            $roster = array();
            $playerHeader = ['Jersey', 'Player', 'Pos', 'Yr', 'GP', 'GS'];

            $table = $crawler->filter('#stat_grid tbody')->filter('tr')->each(function ($tr, $i) {
                return $tr->filter('td')->each(function ($td, $i) {
                    return trim($td->text());
                });
            });

            foreach ($table as $key => $item) {
                $list = array_combine($playerHeader, array_values($item));
                $roster[$key] = $list;
            }
            dd($roster);
            return json_encode($roster);
        }
    */

}
