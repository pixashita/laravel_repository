<?php
namespace App\Http\Controllers;


use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $user;

    public function __construct(UserRepository $userRepository)
    {
        $this->user = $userRepository;
    }

    /**
     * Get all users
     * @return mixed
     */
    public function index()
    {
        return $this->user->findAll();
    }

    /**
     * Create a user
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        return $this->user->create($request->all());
    }

    /**
     * Update user
     * @param Request $request
     * @param $id
     * @return bool|mixed
     */
    public function update(Request $request, $id)
    {
        if (!$id) {
            return false;
        }

        $request->validate([
            'name' => 'required'
        ]);

        return $this->user->update($id, $request->all());
    }

    /**
     * Show user detail by id
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->user->findById($id); // $this->user->findByField('id', $id)

    }

    /**
     * Delete user by id
     * @param $request
     * @return bool|mixed
     */
    public function delete(Request $request)
    {
        $id = $request->get('id');
        if (!$id) {
            return false;
        }

        return $this->user->deleteByField('id', $id);
    }

}