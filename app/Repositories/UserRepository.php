<?php
namespace App\Repositories;


use App\Models\User;

class UserRepository extends BaseRepository
{
    /**
     * Get model
     * @return string
     */
    public function getModel()
    {
        return User::class;
    }

    /**
     * Create new user
     * @param $data
     * @return bool|mixed
     */
    public function create($data)
    {
        //Encrypt password
        if (isset($data['password']) && $data['password'] != null) {
            $data['password'] = bcrypt($data['password']);
        }

        $user = parent::create($data);

        if ($user) {
            $data['user_id'] = $user->id;
            $user->UserDetail()->create($data);

            return $user;
        }

        return false;
    }
}
