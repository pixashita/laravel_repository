<?php
namespace App\Repositories;


abstract class BaseRepository
{
    /**
     * @var $_model
     */
    protected $_model;

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        $this->setModel();
    }

    /**
     * get model
     * @return string
     */
    abstract public function getModel();

    /**
     * Set model
     */
    public function setModel()
    {
        $this->_model = app()->make(
            $this->getModel()
        );
    }

    /**
     * Get model instance
     * @return mixed
     */
    public function model()
    {
        return $this->_model;
    }

    /**
     * Create new model
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        return $this->_model->create($data);
    }

    /**
     * Update model
     * @param $id
     * @param $data
     * @return mixed
     */
    public function update($id, $data)
    {
        return $this->findById($id)->update($data);
    }

    /**
     * Get model by id
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->_model->where('id', $id)->first();
    }

    /**
     * Get all model
     * @return mixed
     */
    public function findAll()
    {
        return $this->_model->all();
    }

    /**
     * Get model by field | $first = true for get one record
     * @param $field
     * @param $value
     * @param $first
     * @return mixed
     */
    public function findByField($field, $value, $first = false)
    {
        //Add condition
        $queryBuilder = $this->_model->where($field, '=', $value);

        if ($first) {
            return $queryBuilder->first();
        }

        return $queryBuilder->get();
    }

    /**
     * Get model by multi fields | $first = true for get one record
     * 1: field name; 2: operator; 3: value
     * @param $conditions
     * @param $first
     * @return mixed
     */
    public function findByMultiFields(array $conditions = [], $first = false)
    {
        $queryBuilder = $this->_model->query();

        //Add conditions
        if (count($conditions) > 0) {
            foreach ($conditions as $condition) {
                if (count($condition) == 2) {
                    $queryBuilder->where($condition[0], $condition[1]);
                } elseif (count($condition) === 3) {
                    $operator = strtoupper($condition[1]);
                    switch ($operator) {
                        case 'IN':
                            $queryBuilder->whereIn($condition[0], $condition[2]);
                            break;
                        case 'NOT_IN':
                            $queryBuilder->whereNotIn($condition[0], $condition[2]);
                            break;
                        case 'OR':
                            $queryBuilder->orWhere($condition[0], $condition[2]);
                            break;
                        default:
                            $queryBuilder->where($condition[0], $operator, $condition[2]);
                            break;
                    }
                }
            }

            if ($first) {
                return $queryBuilder->first();
            }

            return $queryBuilder->get();
        }

        return null;
    }

    /**
     * Find model with specific fields by field and value | $first = true for get one record
     * @param array $selects
     * @param $field
     * @param $value
     * @param bool $first
     * @return mixed
     */
    public function findFieldsByField(array $selects = [], $field, $value, $first = false)
    {
        $queryBuilder = $this->_model->query();

        //Add select
        if (!empty($selects)) {
            foreach ($selects as $select) {
                $queryBuilder->addSelect($select);
            }
        }

        //Add condition
        $queryBuilder->where($field, '=', $value);

        if ($first) {
            return $queryBuilder->first();
        }

        return $queryBuilder->get();
    }

    /**
     * Delete model by field
     * @param $field
     * @param $value
     * @return mixed
     */
    public function deleteByField($field, $value)
    {
        return $this->_model->where($field, $value)->delete();
    }

    /**
     * Delete model by multi fields
     * @param array $conditions
     * @return bool
     */
    public function deleteByFields(array $conditions = [])
    {
        $queryBuilder = $this->_model->query();

        //Add conditions
        if (count($conditions) > 0) {
            foreach ($conditions as $condition) {
                if (count($condition) == 2) {
                    $queryBuilder->where($condition[0], $condition[1]);
                } elseif (count($condition) === 3) {
                    $operator = strtoupper($condition[1]);
                    switch ($operator) {
                        case 'IN':
                            $queryBuilder->whereIn($condition[0], $condition[2]);
                            break;
                        case 'NOT_IN':
                            $queryBuilder->whereNotIn($condition[0], $condition[2]);
                            break;
                        case 'OR':
                            $queryBuilder->orWhere($condition[0], $condition[2]);
                            break;
                        default:
                            $queryBuilder->where($condition[0], $operator, $condition[2]);
                            break;
                    }
                }
            }

            return $queryBuilder->delete();
        }

        return false;
    }
}