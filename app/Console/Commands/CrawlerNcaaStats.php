<?php

namespace App\Console\Commands;

use App\Models\Player;
use App\Models\Team;
use Goutte\Client;
use Illuminate\Console\Command;

class CrawlerNcaaStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CrawlerNcaaStats:crawler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crawl Stats From Ncaa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $divisions = [1, 2, 3];
        $roster = array();

        $client = new Client();

        foreach ($divisions as $division) {

            $teamCrawler = $client->request('GET', 'http://stats.ncaa.org/team/inst_team_list?academic_year=2018&conf_id=-1&division=' . $division . '&sport_code=MBA');

            $teams[$division] = $teamCrawler->filter(' table tr td')->filter('table tr td')->each(function ($td, $i) use (&$division, $roster) {
                $ncaaStatsUrl = 'http://stats.ncaa.org';
                $teamDetailUrl = $td->filter('a')->attr('href');

                $teamName = trim($td->text());

                $teamId = Team::create([
                    'name' => $teamName,
                    'division' => $division,
                    'url' => $teamDetailUrl
                ])->id;

                $rosterUrl = str_replace(basename($teamDetailUrl), 'roster', $teamDetailUrl) . '/' . basename($teamDetailUrl);

                $client = new Client();
                $playerCrawler = $client->request('GET', $ncaaStatsUrl . $rosterUrl);

                $playerHeader = ['jersey', 'name', 'position', 'year', 'gp', 'gs'];

                $playerTable = $playerCrawler->filter('#stat_grid tbody')->filter('tr')->each(function ($tr, $i) {
                    return $tr->filter('td')->each(function ($td, $i) {
                        return trim($td->text());
                    });
                });

                foreach ($playerTable as $key => $item) {

                    $player = array_combine($playerHeader, array_values($item));
                    $player['team_id'] = $teamId;

                    Player::create($player);
                    $roster[$teamName][$key] = $player;
                }

            });
        }
    }
}
