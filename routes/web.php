<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/ncaa', 'NcaaCrawlerController@index');

Route::group(['prefix' => 'user'], function () {
    //Get users
    Route::get('/', 'UserController@index');
    //Create user
    Route::post('/create', 'UserController@create');
    //Update user
    Route::post('/update', 'UserController@update');
    //Delete user
    Route::post('/delete', 'UserController@delete');
    //Show user detail
    Route::get('/{id}', 'UserController@show');
});